```
playbook: users.yml

  play #1 (all): all    TAGS: []
    tasks:
      users : Make a 'sudoers' group    TAGS: [create_sudo_group]
      users : Allow 'sudoers' group to have passwordless sudo   TAGS: [add_sudo_group]
      users : Create user: {{ username }}       TAGS: [create_user]
      users : Create git user - {{ gituser }}   TAGS: [create_gituser]
      users : Create .ssh directory for {{ gituser }}   TAGS: [create_sshdir, create_user]
      users : Manage key for {{ username }} to git      TAGS: [copy_ssh_key, create_user]
      users : Create hosts user - {{ hostuser }}        TAGS: [create_hostuser]
      users : Create a 2048-bit SSH key for user {{ hostuser }} in ~{{ hostuser }}/.ssh/id_rsa  TAGS: [create_hostuser, create_hostuser_ssh_key]
      users : Fetch the key {{ hostuser }} from one server to another   TAGS: [create_hostuser, fetch_pubkey]
      users : Copy the file from master to the destination      TAGS: [create_hostuser]
      users : add the public key into Authorized_keys file to enable Key Auth {{ hostuser }}    TAGS: [add_public_key, create_hostuser]
      users : Create .ssh directory for {{ username }}  TAGS: [create_sshdir, create_user]
      users : Manage key for {{ username }}     TAGS: [copy_ssh_key, create_user]

playbook: pre-playbook.yml

  play #1 (all): all    TAGS: []
    tasks:
      docker : Install aptitude using apt       TAGS: [install_docker]
      docker : Install pip      TAGS: []
      docker : Install docker python package    TAGS: []
      docker : Install required system packages TAGS: [install_docker, install_docker_required]
      docker : Add Docker GPG apt Key   TAGS: [add_gpg_docker, install_docker]
      docker : Add Docker Repository    TAGS: [add_docker_rep, install_docker]
      docker : Install docker-ce        TAGS: [install_docker, install_docker_ce]
      docker : Install docker-ce-cli    TAGS: [install docker_ce_cli, install_docker]
      docker : Install containerd.io    TAGS: [install_containerd, install_docker]
      docker : Install docker-compose   TAGS: [install_compose, install_docker]
      ssh : Copy sshd config    TAGS: [copy_sshd_config, install_ssh]
      git : install git TAGS: [install_git]
      git : Init Git repository - create project folder TAGS: [create_project_folder, init_git]
      git : Init Git repository - init bare     TAGS: [init_bare, init_git]
      git : add the public key into Authorized_keys file to enable Key Auth {{ gituser }}       TAGS: [add_public_key, create_hostuser]

  play #2 (git): git    TAGS: []
    tasks:
      git : install git TAGS: [install_git]
      git : Init Git repository - create project folder TAGS: [create_project_folder, init_git, install_git]
      git : Init Git repository - init bare     TAGS: [init_bare, init_git, install_git]
      git : add the public key into Authorized_keys file to enable Key Auth {{ gituser }}       TAGS: [add_public_key, create_hostuser, install_git]

  play #3 (webservers): webservers      TAGS: []
    tasks:
      Clone project code to webservers  TAGS: [copy_code]

playbook: playbook.yml

  play #1 (docker-registry): docker-registry    TAGS: []
    tasks:
      docker-registry : Create file daemon.json and add insecure parameter      TAGS: [docker_insecure_registry]
      docker-registry : Start the registry container    TAGS: [start_registry]

  play #2 (webservers): webservers      TAGS: []
    tasks:
      Run docker-compose.yml to build project   TAGS: [run_containers]
```
project docker-compose file
```
version: "3.8"
   
services:
  db:
    image: postgres
    volumes:
      - postgres_data:/var/lib/postgresql/data/
    environment:
      - POSTGRES_DB=postgres
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=postgres
    network:
      - dashboard
    
  web:
    restart: always
    volumes:
      - .:/var/dashboard
    build: .
    command: gunicorn core.wsgi:application --bind 0.0.0.0:8000
    expose:
      - 8000
    depends_on:
      - db
    network:
      - dashboard

  nginx:
    build: ./nginx
    ports:
      - 1337:80
    depends_on:
      - web
    network:
      - dashboard
  
volumes:
  postgres_data:    
networks:
  dashboard:
    enable_ipv6: false
```
```
Dockerfile
############################################
FROM python:3
#copy repository

WORKDIR /dashboard_project
RUN pip install -r requirements.txt

EXPOSE 5005
```




![image](/uploads/b32bf99a6957ba129c3248cd6f660c39/image.png)
